#ifndef _Creltek_MCP4xxx_h_
#define _Creltek_MCP4xxx_h_
// ---------------------------------------------------------------------------
// Copyright (C) 2015 Kevin H. Patterson
// Created on 2015-12-27.
//
// @license
// Creltek::MCP4xxx by Kevin H. Patterson is licensed under a Creative Commons
// Attribution-ShareAlike 4.0 International License.
// Based on a work at https://bitbucket.org/kpatterson/creltek_mcp4xxx
//
// This software is furnished "as is", without technical support, and with no
// warranty, express or implied, as to its usefulness for any purpose.
//
// Thread Safe: No
// Extendable: Yes
//
// @file MCP4xxx.h
// Provides a driver for the Microchip MCP4xxx series i2c digital
// potentiometer / digital rheostat ICs.
//
// @dependencies
// Standard Arduino libraries
//
// @author Kevin H. Patterson - kevpatt _at_ khptech _dot_ com
// ---------------------------------------------------------------------------

#include <Wire.h>
#include <inttypes.h>


namespace Creltek {

	class MCP4xxx {
	public:
		MCP4xxx( uint8_t i_Address = 0b0000111 )
		: m_Address( 0b0101000 | (i_Address & 0b0000111) )
		{}

		uint8_t address() const {
			return m_Address;
		}

		bool init() {
			read( 0 );
			read( 1 );
			return true;
		}

		int16_t write( uint8_t i_Wiper, int16_t i_Value ) {
			if( i_Wiper > 1 )
				return false;

			if( i_Value < 0 )
				i_Value = 0;
			else if( i_Value > 256 )
				i_Value = 256;

			uint8_t Bytes[2];
			Bytes[0] = (i_Wiper << 4) | ((i_Value >> 8) & 0b1);
			Bytes[1] = i_Value & 0xff;

			Wire.beginTransmission( m_Address );
			Wire.write( Bytes[0] );
			Wire.write( Bytes[1] );
			Wire.endTransmission();

			m_Wipers[i_Wiper] = i_Value;
			return i_Value;
		}

		int16_t read( uint8_t i_Wiper ) {
			if( i_Wiper > 1 )
				return 0;

			uint8_t Bytes[2];
			Bytes[0] = (i_Wiper << 4) | 0b1100;

			Wire.beginTransmission( m_Address );
			Wire.write( Bytes[0] );
			Wire.endTransmission( false );

			Wire.requestFrom( m_Address, uint8_t( 2 ) );
			if( 2 <= Wire.available() ) {
				Bytes[0] = Wire.read();
				Bytes[1] = Wire.read();
			}

			m_Wipers[i_Wiper] = (Bytes[0] << 8) | Bytes[1];

			return m_Wipers[i_Wiper];
		}

		uint16_t quickRead( uint8_t i_Wiper ) {
			if( i_Wiper > 1 )
				return 0;
			
			return m_Wipers[i_Wiper];
		}
		
	private:
		int16_t m_Wipers[2];
		const uint8_t m_Address;
	};

}

#endif // _MCP4xxx_

